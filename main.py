# -*- coding: utf-8 -*- #
"""
    detect rectangle on image
"""
from __future__ import print_function, absolute_import, with_statement
import pickle

import bson
import cv2
from PIL import Image
import numpy as np
import pymongo


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


IMG = '/Users/fashust/work/counters_samples/2.jpg'
CONNECTION = pymongo.MongoClient(
    host='localhost', port=27017
)['number_detection']


def show_image(img, window_name):
    """
        show image
    """
    cv2.imshow(
        window_name,
        img
        # cv2.resize(img, (640, 480), interpolation=cv2.INTER_LINEAR)
    )


def save_part(img, index):
    """
        save image data to database
    """
    collection = CONNECTION['not_recognized_data']
    collection.save({
        'index': index,
        'img': bson.Binary(pickle.dumps(img, protocol=2)),
        'recognized': False,
        'src': IMG
    })
    return True


def get_train_data():
    """
        get train data for recognition
    """
    collection = CONNECTION['not_recognized_data']
    train_data = collection.find(
        {'recognized': True},
        fields={'_id': False, 'result': True, 'index': True, 'img': True}
    )
    d = map(
        lambda x: (x['result'], pickle.loads(x['img']).reshape(90*50).astype(np.float32)),
        train_data
    )
    # print(zip(*d))
    return zip(*d)


def run():
    """
        main
    """
    im_size = (600, 120)
    src_img = cv2.imread(IMG)
    operating_img = cv2.resize(src_img, im_size)
    gray_img = cv2.cvtColor(operating_img, cv2.COLOR_BGR2GRAY)
    blured = cv2.blur(gray_img, (3, 3))
    bedges = cv2.Canny(blured, 100, 200)
    contours, hierarchy = cv2.findContours(bedges, cv2.cv.CV_RETR_EXTERNAL, cv2.cv.CV_CHAIN_APPROX_SIMPLE)
    ap_contours = []
    for c in contours:
        ap_contours.append(cv2.approxPolyDP(c, 0.1 * cv2.arcLength(c, True), True))
    biggest_rect = None
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        if 200 < w * h:
            if biggest_rect is None:
                biggest_rect = x, y, w, h
            elif w * h > biggest_rect[2] * biggest_rect[3]:
                biggest_rect = x, y, w, h
            # cv2.rectangle(operating_img, (x, y), (x+w, y+h), (0, 255, 0), 1)
    x, y, w, h = biggest_rect
    # cv2.rectangle(operating_img, (x, y), (x+w, y+h), (0, 255, 0), 1)
    im = Image.fromarray(np.uint8(cv2.cvtColor(operating_img, cv2.COLOR_BGR2RGB)))
    c_im = im.crop(biggest_rect)
    operating_img = cv2.cvtColor(np.asarray(c_im), cv2.COLOR_RGB2BGR)
    operating_img = cv2.resize(operating_img, im_size)
    del(im)
    del(c_im)
    # show_image(operating_img, 'all contours')

    gray_img = cv2.cvtColor(operating_img, cv2.COLOR_BGR2GRAY)
    blured = cv2.blur(gray_img, (3, 3))
    bedges = cv2.Canny(blured, 100, 200)
    contours, hierarchy = cv2.findContours(bedges, cv2.cv.CV_RETR_EXTERNAL, cv2.cv.CV_CHAIN_APPROX_SIMPLE)
    ap_contours = []
    # for c in contours:
    #     ap_contours.append(cv2.approxPolyDP(c, 0.1 * cv2.arcLength(c, True), True))
    rects = {}
    i = 0
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        if w * h > 10:
            rects[i] = {
                'x': x,
                'y': y,
                'w': w,
                'h': h,
                'area': w * h
            }
            i += 1
    max_area = max(rects.items(), key=lambda x: x[1]['area'])
    min_area = min(rects.items(), key=lambda x: x[1]['area'])
    rects = dict([x for x in rects.items() if x not in [max_area, min_area]])
    image_parts = []
    # block_size = (20, 40)
    i = 0
    for item in rects.items():
        x, y, w, h = item[1]['x'], item[1]['y'], item[1]['w'], item[1]['h']
        if h > w:
            tmp_im = Image.fromarray(np.uint8(cv2.cvtColor(operating_img, cv2.COLOR_BGR2RGB)))
            tmp_im = tmp_im.crop((x, y, x+w, y+h))
            tmp_im = cv2.cvtColor(np.asarray(tmp_im), cv2.COLOR_RGB2BGR)
            h, w, _ = tmp_im.shape
            # show_image(tmp_im, '{}'.format(float(h) / float(w)))
            # show_image(tmp_im, '{}'.format(float(h) / float(w)))
            # image_parts.append(tmp_im)
            if h * w > 800:
                # cv2.rectangle(operating_img, (x, y), (x+w, y+h), (0, 255, 0), 1)
                tmp_im = cv2.resize(
                    cv2.threshold(
                        cv2.cvtColor(tmp_im, cv2.COLOR_BGR2GRAY),
                        128,
                        255,
                        cv2.THRESH_BINARY
                    )[1],
                    (50, 90)
                )
                show_image(
                    tmp_im,
                    '{}'.format(','.join([str(x) for x in (x, y, w, h)]))
                )
                # print(h * w)
                i += 1
                # save_part(tmp_im, i) # todo: uncomment for saving
                image_parts.append(tmp_im)
    show_image(operating_img, 'all contours')
    knn = cv2.KNearest()
    results, train_data = get_train_data()
    knn.train(np.array(train_data), np.array(results))
    test_data = image_parts[4]
    show_image(test_data, 'test_data')
    test_data = test_data.reshape(90*50).astype(np.float32)
    ret, result, neighbours, dist = knn.find_nearest(np.array([test_data]), k=5)
    print(ret)
    print(result)
    print(neighbours)
    print(dist)
    # print(image_parts)

    done = False
    while not done:
        key = cv2.waitKey(10)
        if key == 27:
            done = True
    cv2.destroyAllWindows()


if __name__ == '__main__':
    run()