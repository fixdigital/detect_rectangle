# -*- coding: utf-8 -*-
"""
    generate data for initial training
"""
import pickle
import numpy as np
import pymongo
import cv2


__author__ = 'fashust'
__email__ = 'fashust.nefor@gmail.com'


def main():
    """
        main
    """
    connection = pymongo.MongoClient(
        host='localhost', port=27017
    )['number_detection']
    collection = connection['not_recognized_data']
    for item in list(collection.find({'recognized': False}))[:]:
        np_data = pickle.loads(item['img'])
        cv2.imshow(str(str(item['_id'])), np_data)
        result = raw_input('result for item {}: '.format(item['_id']))
        item['recognized'] = True #if result != '?' else False
        #if item['recognized']:
        item['result'] = int(result) #if item['recognized'] else result
        collection.save(item)
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
